# Text Cycle

## About

### Author

Ryan E. Anderson

---

### Description

This is a small library that contains methods for cycling through text.

---

### Version

0.1.0

---

### License

Apache-2.0

---

## Library

### cycleText

This method can be used to repeatedly cycle through a given array and assign each
element to the inner text of a target element that is specified by a given selector.
A user may also specify the length of each interval.

```js
cycleText(".some-class > #some-text-element", ["hello", "world"], 3000);
```

### getTextCycleEngine

This is the core mechanism for cycling.

```js
const textElement = document.querySelector(".some-class > #some-text-element");

setTimeout(getTextCycleEngine(textElement, ["hello", "world"]), 3000);
```